package com.binar.entities.enemies;

import com.binar.entities.Character;

public class Enemies extends Character {

    private  String armorAttribute ;
    private int armorHealth;
    private int attPower;

    public Enemies(String name,int armorHealth, int attPower, String armorAtribute) {
        super(name);
        this.armorHealth = armorHealth;
        this.attPower = attPower;
        this.armorAttribute = armorAtribute;

    }

    public String getArmorAttribute() {
        return armorAttribute;
    }

    public int getArmorHealth() {
        return armorHealth;
    }

    public int getAttPower() {
        return attPower;
    }
}
