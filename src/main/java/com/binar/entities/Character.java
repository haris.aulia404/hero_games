package com.binar.entities;

public class Character {

    private String name;
    private int baseHealth = 100;

    public Character(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBaseHealth(int baseHealth) {
        this.baseHealth = baseHealth;
    }

    public int getBaseHealth() {
        return baseHealth;
    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", baseHealth=" + baseHealth +
                '}';
    }
}
