package com.binar.entities.heroes;

import com.binar.entities.Character;

public class Heroes extends Character {

    private String heroAttribute;
    private int armorHealth;
    private int baseAttackPower = 40;

    public Heroes(String name, String heroAttribute, int armorHealth) {
        super(name);
        this.heroAttribute = heroAttribute;
        this.armorHealth = armorHealth;
    }

    public String getHeroAttribute() {
        return heroAttribute;
    }

    public int getArmorHealth() {
        return armorHealth;
    }

    public int getBaseAttackPower() {
        return baseAttackPower;
    }

    @Override
    public String toString() {
        return "Heroes{" +
                "heroAttribute='" + heroAttribute + '\'' +
                ", armorHealth=" + armorHealth +
                ", baseAttackPower=" + baseAttackPower +
                '}';
    }
}
