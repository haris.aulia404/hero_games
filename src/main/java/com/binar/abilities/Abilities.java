package com.binar.abilities;

public interface Abilities <T> {

    int hit(T character);

    int getHealth (T character);

}
