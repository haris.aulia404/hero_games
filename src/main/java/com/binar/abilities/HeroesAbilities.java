package com.binar.abilities;

import com.binar.entities.heroes.Heroes;

public class HeroesAbilities implements Abilities<Heroes> {

    @Override
    public int hit(Heroes heroes) {
        return heroes.getBaseAttackPower();
    }

    @Override
    public int getHealth(Heroes heroes) {
        int totalHealth = heroes.getBaseHealth() + heroes.getArmorHealth();
        heroes.setBaseHealth(totalHealth);
        return totalHealth;
    }
}
