package com.binar.abilities;

import com.binar.entities.enemies.Enemies;

public class EnemiesAbilitites<T> implements Abilities<Enemies> {
    @Override
    public int hit(Enemies enemies) {
        return enemies.getAttPower();
    }

    @Override
    public int getHealth(Enemies enemies) {
        int totalHealth = enemies.getBaseHealth() + enemies.getArmorHealth();
        enemies.setBaseHealth(totalHealth);
        return totalHealth;
    }
}
