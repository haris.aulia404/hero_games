package com.binar;

import com.binar.view.MainMenu;

/**
 * Created By Harisatul Aulia
 * 4-8-2022
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        MainMenu mainMenu = new MainMenu();
        mainMenu.show();
    }
}
