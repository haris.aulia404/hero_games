package com.binar.map;

import com.binar.abilities.Abilities;
import com.binar.abilities.EnemiesAbilitites;
import com.binar.abilities.HeroesAbilities;
import com.binar.entities.enemies.Enemies;
import com.binar.entities.heroes.Heroes;

import java.util.Scanner;

public class TitanWorld extends World{

    private Abilities heroesAbilities;
    private Abilities enemiesAbilities;

    public TitanWorld(String name) {
        super(name);
        this.heroesAbilities = new HeroesAbilities();
        this.enemiesAbilities = new EnemiesAbilitites();
    }

    @Override
    public void worldLogic(Heroes heroes) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nKamu akan melawan salah satu dari enemies berikut ini :  ");
        for (Enemies enemies: getLIST_OF_ENEMIES()) {
            System.out.println(enemies.getName() + " - " +
                    "\nAtrribute : " + enemies.getArmorAttribute() +
                    "\nBase Health : " + enemies.getBaseHealth() +
                    "\nAttack Power : " + enemies.getAttPower());
        }
        System.out.println("\neitss, kamu tidak bisa memilih karena akan digacha hahaha");
        System.out.print("Tekan 'y' untuk menggacha  : ");
        scanner.nextLine();
        Enemies choosenEnemyCharacter =  getChoosenEnemyCharacter();
        System.out.println("Hayooo, kamu akan ngelawan " + choosenEnemyCharacter.getName());
        System.out.println("--------------------------------------");
        System.out.println("gamess nya angat sederhana, kamu hanya harus menebak angka yang tepat dari 1-50.");
        System.out.println("apabila angka yang anda tebak benar. Maka, hero kamu akan nembak abilitynya ke villain");
        System.out.println("Tapi, kalo angka yang kamu tebak salah. Maka, hero kamu akan tembakan dari villainnya");

        System.out.println("okeee, siaaap yaaaa");
        System.out.print("Tekan 'y' untuk mulai  : ");
        scanner.nextLine();



        int hitPower = heroesAbilities.hit(heroes);
        int healthHeroes = heroesAbilities.getHealth(heroes);

        int enemyHit = enemiesAbilities.hit(choosenEnemyCharacter);
        int enemyHealth = enemiesAbilities.getHealth(choosenEnemyCharacter);


        while (healthHeroes > 0 && enemyHealth > 0) {

            boolean correct = false;
            int rand = (int) (Math.random() * 50) + 1;
            while (!correct) {
                if (healthHeroes <= 0 )
                    break;
                System.out.print("\nCoba tebak angkanya dari 1-50: ");

                int  guess = scanner.nextInt();

                if (guess > rand) {
                    healthHeroes -= enemyHit;
                    heroes.setBaseHealth(healthHeroes);
                    System.out.println("\nPfttt... Terlalu tinggi, coba lagi");
                    System.out.println("sisa health hero : "+healthHeroes  + ", " +
                            "sisa health enemy  : " + enemyHealth );
                }

                else if (guess < rand) {
                    healthHeroes -= enemyHit;
                    heroes.setBaseHealth(healthHeroes);
                    System.out.println("\nHadeeh.....Terlalu rendah, coba lagi");
                    System.out.println("sisa health hero : "+healthHeroes  + ", " +
                            "sisa health enemy  : " + enemyHealth );
                }

                else {
                    System.out.println("\nuhuyyy, dapet nih nomornya.");
                    System.out.println("menyeraang villain .... ");
                    enemyHealth -= hitPower;
                    choosenEnemyCharacter.setBaseHealth(enemyHealth);
                    System.out.println("duar villainnya kena hit .... ");
                    System.out.println("sisa health hero : "+healthHeroes  + ", " +
                            "sisa health enemy  : " + enemyHealth );
                    correct = true;
                }
            }
        }

        if (healthHeroes <= 0){
            System.out.println("\nkamu kalah wleeee");
            System.out.println("sisa health hero : "+healthHeroes  + ", " +
                    "sisa health enemy  : " + enemyHealth );
        }else {
            System.out.println("kamu menang");
        }


    }
}
