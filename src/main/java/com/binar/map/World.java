package com.binar.map;

import com.binar.entities.Character;
import com.binar.entities.enemies.Enemies;
import com.binar.entities.enemies.RedSkull;
import com.binar.entities.enemies.Thanos;
import com.binar.entities.heroes.CaptainAmerica;
import com.binar.entities.heroes.Heroes;
import com.binar.entities.heroes.IronMan;

import java.util.List;

public abstract class World {

    private String name;
    private final List<Heroes> LIST_OF_HEROES = List.of(new IronMan("Iron Man"),
            new CaptainAmerica("Captain America"));
    private final List<Enemies> LIST_OF_ENEMIES =
            List.of(new Thanos("Thanos"), new RedSkull("RedSkull"));
    private Heroes heroCharacter;
    private Enemies enemyCharacter;

    public World(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Heroes> getLIST_OF_CHAR() {
        return LIST_OF_HEROES;
    }

    public List<Enemies> getLIST_OF_ENEMIES() {
        return LIST_OF_ENEMIES;
    }

    public void setEnemyCharacter(Enemies enemyCharacter) {
        this.enemyCharacter = enemyCharacter;
    }

    public void setChooseCharacter (int choosen){
        if (choosen == 1){
            heroCharacter = LIST_OF_HEROES.get(0);
        }else if (choosen == 2){
            heroCharacter = LIST_OF_HEROES.get(1);
        }
    }

    public Heroes getChoosenHeroCharacter() {
        return heroCharacter;
    }

    public Enemies getChoosenEnemyCharacter() {
        int rand = (int) (( Math.random() * 2 ) + 0);
        Enemies character = LIST_OF_ENEMIES.get(rand);
        setEnemyCharacter(character);
        return character;
    }

    abstract public void worldLogic(Heroes heroes);
}
