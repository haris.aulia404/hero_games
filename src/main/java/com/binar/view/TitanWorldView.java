package com.binar.view;

import com.binar.entities.heroes.Heroes;
import com.binar.map.TitanWorld;
import com.binar.map.World;

import java.util.Scanner;

public class TitanWorldView {

    private World titandWorld ;
    static Scanner scanner = new Scanner(System.in);

    public TitanWorldView() {
        this.titandWorld = new TitanWorld("Titan World");
    }

    public void show(){

        welcomeMessage();

        System.out.println("--------------------------");
        System.out.println("   Choose your Character  ");
        System.out.println(" 1. Iron Man              ");
        System.out.println(" 2. Captain America       ");
        System.out.println("--------------------------");

        System.out.print("masukkan pilihan : ");
        while (!scanner.hasNextInt()){
            System.out.print("inputan salah. tolong masukkan angka : ");
            scanner.nextLine();
        }
        int choice = scanner.nextInt();

        titandWorld.setChooseCharacter(choice);
        Heroes choosenCharacter =  titandWorld.getChoosenHeroCharacter();


        System.out.println("----------------------------------------------");
        System.out.println("kamu memilih karakter  " + choosenCharacter.getName() + " dengan atribut " +
                choosenCharacter.getHeroAttribute());
        System.out.println(" -- detail karakter ---" + '\n' +
                "base Health    : " + choosenCharacter.getBaseHealth() + '\n' +
                "armor Health   : " + choosenCharacter.getArmorHealth() + '\n' +
                "attack Power   : " + choosenCharacter.getBaseAttackPower() );

       titandWorld.worldLogic(choosenCharacter);

        MapMenu mapMenu = new MapMenu();
        System.out.print("mau lanjut bermain ? (y/n) : " );
        String anotherString = scanner.next();
        if (anotherString.equalsIgnoreCase("y")){
            mapMenu.show();
        }
        else{
            System.exit(0);
        }


    }


    public static void welcomeMessage(){
        System.out.println("-----------------------------------------------");
        System.out.println("            selamat datang di Titan            ");
        System.out.println("-----------------------------------------------");
        System.out.println(" Tempat tinggal thanos. tapi udah hancur gan   ");
        System.out.println(" Avengersss pernah kalaah disni wkwkwkwk       ");
        System.out.println("-----------------------------------------------");

    }
}
