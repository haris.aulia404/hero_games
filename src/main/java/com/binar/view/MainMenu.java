package com.binar.view;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MainMenu {

    static Scanner scanner = new Scanner(System.in);
    private MapMenu mapMenu;

    public MainMenu() {
        mapMenu = new MapMenu();
    }

    public void show(){

        System.out.println("------------------");
        System.out.println("   Main Menu      ");
        System.out.println(" 1. play games    ");
        System.out.println(" 2. quit games    ");
        System.out.println("------------------");

        System.out.print("masukkan pilihan : ");
        while (!scanner.hasNextInt()){
            System.out.print("inputan salah. tolong masukkan angka : ");
            scanner.nextLine();
        }
        int choice = scanner.nextInt();

        switch (choice){
            case 1 -> {
                System.out.print("\033[H\033[2J");
                System.out.flush();
                mapMenu.show();
            }case 2 -> {
                System.out.println("Have a nice day. Good bye");
                System.exit(0);
            }
        }

    }
}
