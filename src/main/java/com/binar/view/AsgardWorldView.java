package com.binar.view;

import com.binar.entities.Character;
import com.binar.entities.heroes.Heroes;
import com.binar.entities.heroes.IronMan;
import com.binar.map.AsgardWorld;
import com.binar.map.World;

import java.util.Scanner;

public class AsgardWorldView {

    private World asgardWorld ;
    static Scanner scanner = new Scanner(System.in);

    public AsgardWorldView() {
        this.asgardWorld = new AsgardWorld("World of champion");
    }

    public void show(){

        welcomeMessage();

        System.out.println("--------------------------");
        System.out.println("   Choose your Character  ");
        System.out.println(" 1. Iron Man              ");
        System.out.println(" 2. Captain America       ");
        System.out.println("--------------------------");

        System.out.print("masukkan pilihan : ");
        while (!scanner.hasNextInt()){
            System.out.print("inputan salah. tolong masukkan angka : ");
            scanner.nextLine();
        }
        int choice = scanner.nextInt();

        asgardWorld.setChooseCharacter(choice);
        Heroes choosenCharacter =  asgardWorld.getChoosenHeroCharacter();


        System.out.println("----------------------------------------------");
        System.out.println("kamu memilih karakter  " + choosenCharacter.getName() + " dengan atribut " +
                            choosenCharacter.getHeroAttribute());
        System.out.println(" -- detail karakter ---" + '\n' +
                            "base Health    : " + choosenCharacter.getBaseHealth() + '\n' +
                            "armor Health   : " + choosenCharacter.getArmorHealth() + '\n' +
                            "attack Power   : " + choosenCharacter.getBaseAttackPower() );

        asgardWorld.worldLogic(choosenCharacter);

        MapMenu mapMenu = new MapMenu();
        System.out.print("mau lanjut bermain ? (y/n) : " );
        String anotherString = scanner.next();
        if (anotherString.equalsIgnoreCase("y")){
            mapMenu.show();
        }
        else{
            System.exit(0);
        }



    }

    public static void welcomeMessage(){
        System.out.println("-----------------------------------------------");
        System.out.println("            selamat datang di Asgard           ");
        System.out.println("-----------------------------------------------");
        System.out.println(" Ini adalah arena para dewa. Tak mengenal kata ");
        System.out.println(" kalah. Kalahkan lawanmu dan raihlah kemenangan");
        System.out.println("-----------------------------------------------");

    }


}
