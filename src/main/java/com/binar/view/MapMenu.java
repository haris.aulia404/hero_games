package com.binar.view;

import java.util.Scanner;

public class MapMenu {


    static Scanner scanner = new Scanner(System.in);
    private AsgardWorldView asgardWorldView;
    private TitanWorldView titanWorldView;

    public MapMenu() {
        this.asgardWorldView = new AsgardWorldView();
        this.titanWorldView = new TitanWorldView();
    }

    public void show() {
        System.out.println("------------------");
        System.out.println("   Choose Map     ");
        System.out.println(" 1. Asgard        ");
        System.out.println(" 2. Titan         ");
        System.out.println("------------------");

        System.out.print("masukkan pilihan : ");
        while (!scanner.hasNextInt()){
            System.out.print("inputan salah. tolong masukkan angka : ");
            scanner.next();
        }
        int choice = scanner.nextInt();


        switch (choice){
            case 1 -> {
                System.out.print("\033[H\033[2J");
                System.out.flush();
                asgardWorldView.show();
            }case 2 -> {
                System.out.print("\033[H\033[2J");
                System.out.flush();
                titanWorldView.show();
            }default -> {throw new RuntimeException("error....pilih yang bener gan");}
        }


    }
}
